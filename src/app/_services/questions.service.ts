import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from '../_models/question';


@Injectable({
  providedIn: 'root'
})
export class QuestionsService {
  url = 'https://opentdb.com/api.php?amount=10&type=multiple';
  questions: Question[];
  constructor(private http: HttpClient) {
    this.getQuestions();
   }

  getQuestions() {
    this.http.get(this.url).subscribe(
      questions => {
        questions['results'] !== undefined && questions['results'] !== undefined ?
          this.questions = questions['results'] : console.log(questions);
          this.questions.forEach((ques) => {
            ques.incorrect_answers.push(ques.correct_answer);
            // ques.incorrect_answers = this.shuffle(ques.incorrect_answers);
          }
        );
        console.log(this.questions);
        },
      error => console.log(error)
    );
  }

  getQuestion(number: number): Question {
    console.log('get question number ' + number );
    return this.questions[number];
  }

  shuffle(array: string[]): string[] {
    const temp = [];
    for (let i = 0; i < array.length ; i++) {
      temp.push(array.splice(Math.floor(Math.random() * array.length), 1));
    }
    return temp;
  }
}
