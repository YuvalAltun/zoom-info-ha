import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './_pages/home/home.component';
import { QuestionComponent } from './_pages/question/question.component';
import { WinnerComponent } from './_pages/winner/winner.component';
import { ProgressComponent } from './_components/progress/progress.component';
import { HeaderComponent } from './_components/header/header.component';
import { FooterComponent } from './_components/footer/footer.component';
import { AnswerComponent } from './_components/answer/answer.component';
import { SetBackgroundColorOnHoverDirective } from './_directives/set-background-color-on-hover.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    QuestionComponent,
    WinnerComponent,
    ProgressComponent,
    HeaderComponent,
    FooterComponent,
    AnswerComponent,
    SetBackgroundColorOnHoverDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
