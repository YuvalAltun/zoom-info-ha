import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appSetBackgroundColorOnHover]'
})
export class SetBackgroundColorOnHoverDirective {
  @Input('appSetBackgroundColorOnHover') backgroundColor: string;
  isClicked = false;
  constructor(private el: ElementRef) { }


  @HostListener('click') onClick() {
    this.isClicked = true;
  }
  @HostListener('mouseenter') onMouseEnter() {
    this.changeBackgroundColor(this.backgroundColor);
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (!this.isClicked) {
      this.changeBackgroundColor(null);
    }
  }

  private changeBackgroundColor(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

}
