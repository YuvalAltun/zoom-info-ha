import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_pages/home/home.component';
import { QuestionComponent } from './_pages/question/question.component';
import { WinnerComponent } from './_pages/winner/winner.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, data: {animation: 'HomePage'}},
  {path: 'question/:number', component: QuestionComponent, data: {animation: 'QuestionPage'}},
  {path: 'winner', component: WinnerComponent, data: {animation: 'QuestionPage'}},
  {path: '**', redirectTo: 'home'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
