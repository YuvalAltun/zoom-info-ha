import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {
  @Input() answer: string;
  @Input() isSelected: boolean;
  @Input() image: string;
  constructor() { }

  ngOnInit() {
  }

  getImage() {
    if (this.image && this.image === 'success') {
      return 'assets/Group.png';
    } else if (this.image && this.image === 'error') {
      return 'assets/Group 3.png';
    } else {
      return '';
    }
  }
}
