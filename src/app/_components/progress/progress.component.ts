import { Component, OnInit, Input } from '@angular/core';
import { Question } from '../../_models/question';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {
   @Input() questionNumber: number;
   numArray = new Array(10);

  constructor() { }

  ngOnInit() {
    console.log(this.questionNumber);
    console.log(this.numArray);
  }

  isSmallerOrEquel(x: number, y: number): boolean {
    return x <= y;
  }

}
