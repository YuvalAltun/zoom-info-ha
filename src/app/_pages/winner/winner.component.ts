import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../_services/questions.service';

@Component({
  selector: 'app-winner',
  templateUrl: './winner.component.html',
  styleUrls: ['./winner.component.css']
})
export class WinnerComponent implements OnInit {

  constructor(private questionService: QuestionsService) { }

  ngOnInit() {
  }

}
