import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../_services/questions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '../../_models/question';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  question: Question;
  questionNumber: number;
  selectedAnswer: string;
  selectedIndex: number;
  continueButtonText = 'OK';
  private selected: boolean[] = [false, false, false, false];
  private images: string[] = ['', '', '', ''];
  btncolor = '#F68034';
  constructor(
    private questionService: QuestionsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.getQuestion();
  }

  getQuestion(): void {
    this.questionNumber = +this.route.snapshot.paramMap.get('number');
    this.question = this.questionService.getQuestion(this.questionNumber - 1);
  }
  select(selectedIndex: number, selectedAnswer: string): void {
    this.selectedAnswer = selectedAnswer;
    this.selectedIndex = selectedIndex;
    for (let index = 0; index < this.selected.length; index++) {
      if (this.question.incorrect_answers[index] === selectedAnswer) {
        this.selected[index] = true;
      } else {
        this.selected[index] = false;
      }
    }
  }
  submit(): void {
    switch (this.continueButtonText) {
      case 'OK': {
        if (this.selectedAnswer === this.question.correct_answer) {
          this.images[this.selectedIndex] = 'success';
          this.continueButtonText = 'Continue';

        } else {
          this.images[this.selectedIndex] = 'error';
          this.continueButtonText = 'New Game';
        }
        break;
      }
      case 'Continue': {
        if (this.questionNumber === 10) {
          this.questionService.getQuestions();
          this.router.navigate(['/winner']);
          break;
        } else {
          this.router.navigate(['question', (this.questionNumber + 1)]);
          break;
        }
      }
      default: {
        this.questionService.getQuestions();
        this.router.navigate(['/home']);
        break;
      }
    }
  }

}
